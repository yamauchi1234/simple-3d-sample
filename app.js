import React, {Component} from 'react';
import {render} from 'react-dom';

import {StaticMap} from 'react-map-gl';
import DeckGL from '@deck.gl/react';
import {LineLayer} from '@deck.gl/layers';

// Set your mapbox token here
const MAPBOX_TOKEN = process.env.MAPBOX_ACCESS_TOKEN;

// Source data CSV
const DATA = 
    [{"sourcePosition":[136.963551,35.163859,30],"targetPosition":[136.966497,35.154370,50]}];

const INITIAL_VIEW_STATE = {
  latitude: 35.155162, longitude: 136.966106,
  zoom: 15.0, maxZoom: 18, pitch: 50, bearing: 0
};

function hsvToRgb(H,S,V) {
  const C = V * S;
  const Hp = H / 60;
  const X = C * (1 - Math.abs((Hp % 2) - 1));

  let R, G, B;
  if (Hp >= 0 && Hp < 1) { [R, G, B] = [C, X, 0]; }
  if (Hp >= 1 && Hp < 2) { [R, G, B] = [X, C, 0]; }
  if (Hp >= 2 && Hp < 3) { [R, G, B] = [0, C, X]; }
  if (Hp >= 3 && Hp < 4) { [R, G, B] = [0, X, C]; }
  if (Hp >= 4 && Hp < 5) { [R, G, B] = [X, 0, C]; }
  if (Hp >= 5 && Hp < 6) { [R, G, B] = [C, 0, X]; }

  const m = V - C;
  [R, G, B] = [R + m, G + m, B + m];

  R = Math.floor(R * 255);
  G = Math.floor(G * 255);
  B = Math.floor(B * 255);

  return [R, G, B];
}

function getColor(d) {
  return hsvToRgb(d,1,1);
}

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      color : 180,
      width : 3
    };
  }

  _renderLayers() {
    const { data = DATA } = this.props;
    const { visible, color, width } = this.state;

    return [
      new LineLayer({
        visible,
        id: 'sample-line',
        data: data,
        getSourcePosition: d => d.sourcePosition,
        getTargetPosition: d => d.targetPosition,
        getColor: getColor(color),
        getWidth: width,
        widthUnits: 'meters',
        widthMinPixels: 0.1,
      })
    ];
  }

  render() {
    const {mapStyle = 'mapbox://styles/mapbox/dark-v9'} = this.props;
    const { visible, color, width } = this.state;

    return (
      <div>
        <div className='panel'>
          <ul>
            <li>visible:{`${visible?"TRUE":"FALSE"}`}</li>
            <li>
              <input type="checkbox" defaultChecked={true}
              onChange={e => this.setState({visible: e.target.checked})}
              />
            </li>
            <li>width:{`${width}`}</li>
            <li>
              <input type="range" value={width}
              min={0} max={100} step={1}
              onChange={e => this.setState({width: Number(e.target.value)})}
              />
            </li>
            <li>color:{`${getColor(color)}`}</li>
            <li>
              <input type="range" value={color}
              min={0} max={359} step={1}
              onChange={e => this.setState({color: Number(e.target.value)})}
              />
            </li>
          </ul>
        </div>
        <div>
          <DeckGL
            layers={this._renderLayers()}
            initialViewState={INITIAL_VIEW_STATE}
            controller={true}
          >
            <StaticMap
              mapStyle={mapStyle}
              mapboxApiAccessToken={MAPBOX_TOKEN}
            />
        </DeckGL>
        </div>
     </div>
    );
  }
}

export function renderToDOM(container) {
  render(<App />, container);
}
