This is a minimal standalone version of the LineLayer example

### Usage

Copy the content of this folder to your project. 

To see the base map, you need a [Mapbox access token](https://docs.mapbox.com/help/how-mapbox-works/access-tokens/). You can either set an environment variable:

```bash
export MAPBOX_ACCESS_TOKEN=<mapbox_access_token>
```

Or set `MAPBOX_TOKEN` directly in `app.js`.

Other options can be found at [using with Mapbox GL](https://github.com/uber/deck.gl/blob/master/docs/get-started/using-with-map.md).

```bash
# install dependencies
npm install
# or
yarn
# bundle and serve the app with webpack
npm start
```

### Data format
the [documentation of LineLayer](https://github.com/uber/deck.gl/blob/master/docs/layers/line-layer.md).
